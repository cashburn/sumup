'use strict';

var should        = require('chai').should();
var PreProcessing = require('../lib/pre-processing');
var Summarize     = require('../lib/summarize');
var Sumup         = require('../'); 
var assert        = require('assert');
var input         = require('./text.js');

describe('Summarization', function (){
  describe('Document pre-processing', function(){

    before(function(){
      this.config = { language      : 'ENG',
                      summarySize   : 3,
                      normalisation : '',
                    };
      this.input = input.text;
    });
  
    describe('Porter stemmer', function(){
      before(function(done){
        var self = this;
        self.config.normalization = 'PORTER';
        self.preProcessing = new PreProcessing(self.config);
        self.preProcessing.ProcessText(self.input, function(err, res){
          if (err)
            console.log(err);
          else{
            self.output = res;
          }
          done();
        });
      });  
    
      it('should split/filter/normalize the text and create a text matrix', function(){
        var self = this;
        should.exist(self.output.split);
        should.exist(self.output.matrix);
        assert(self.output.matrix.length === self.output.split.length);
      });
    });
    
    describe('Ultra-stemmatization', function(){
      it('should use Ultra-stemmatization to normalize the text', function(){
        //TODO
      });
    });
  });
  describe('Text Summarization Algorithme', function(done){
    before(function(done){
      var self = this;
      self.config = { language      : 'ENG',
                      summarySize   : 3,
                      normalization : 'PORTER',
                      ATS           : ''
                    };
      self.input = input.text.toString();
      self.preProcessing = new PreProcessing(self.config);
      self.preProcessing.ProcessText(self.input, function(err, res){
        if (err) {
          console.log(err);
        }
        else {
          self.output = res;
          done();
        }
        
      });
    });
    
    describe('Artex', function(done){
      before(function(done){
        var self = this;
        self.config.ATS = 'ARTEX';
        
        console.log(self.config);
        self.summarize = new Summarize(self.config);
        self.summarize.ProcessText(self.output, function(error, result){
          if (error) {
            console.log(error);
          } else {
            self.output = result;
            done();
          }
        });
      });  
      
      it('should use ARTEX algo to create a Text', function(done){
        var self = this;
        should.exist(self.output.scores);
        should.exist(self.output.summary);
        console.log(self.output.summary);
        done();
      });
    });
    
    describe('Cortex', function(){
    
      it('should use CORTEX algo to create a Text', function(){
        //TODO
      });
    });
    
    describe('Enertex', function(){
      it('should use ENERTEX algo to create a Text', function(){
        //TODO
      });
    });
    
    describe('Neo-Cortex', function(){
      it('should use Neo-Cortex algo to create a Text from multiple documents', function(){
        //TODO
      });
    });
  });
  
  describe('Sumup API', function(done){
    before(function(){
      var self = this;
      self.config = { language      : 'ENG',
                      summarySize   : 3,
                      normalisation : 'PORTER',
                      ATS           : 'ARTEX'
                    };
      self.input = input.text;
      self.sumup = new Sumup(self.config);
      self.sumup.ProcessText(self.input, function(err, res){
        if (err) console.log(err);  
        self.output = res;
      });
    });
    it('Should pre-process the text and return the matrix and the splitted text', function(done){
      var self = this;
      should.exist(self.output.split);
      should.exist(self.output.matrix);
      assert(self.output.matrix.length === self.output.split.length);
      done();
    });
    it('Should pre-process the text and return the matrix and the splitted text', function(done){
      var self = this;
      should.exist(self.output.scores);
      should.exist(self.output.summary);
      done();
    });
  });
});