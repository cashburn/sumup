# seed-sumup
Summarizes text using a ARTEX, CORTEX or ENERTEX summarization algorithms.


# Installation

    $ npm install npm install git+ssh://bitbucket.org/cashburn/sumup

# Example

```javascript
var Sumup = require('seed-sumup');


var config = { language      : 'ENG',
               summarySize   : 3,
               normalisation : 'PORTER',
               ATS           : 'ARTEX'
             };
             
var sumup = new Sumup(config);


var content = "SKENE’S SCENE\n02 June 2014\nNever underestimate risk\nToday’s belief of minimal risk in financial markets contrasts sharply with reality. Financial risk is\nthe greatest in a century.\nGovernments today mimic the hierarchical and bureaucratic corporate model of the 1930s that\nwas virtually immune to change. Declining real incomes plus unchanged policies are inciting\ncitizen frustrations with leviathan and dysfunctional governments. For example, bail-outs don’t\nbenefit the citizens of the recipient nations. Only creditors that should have been suffering\nhaircuts for their injudicious lending have benefited. Moreover, effectively coping with rising\ndependency ratios and credit bubbles requires functional government – e.g. Singapore. Its public\nspending is about 17% of GDP and about 90% of the benefits received are linked to contributions.\nThe other 10% are a safety net for the very poor and very ill. DM political rhetoric is shifting in that\ndirection, but no action will occur until ‘Belief in Central Bank Omnipotence’ (Belief) ends.\nBelief means everyone ‘knows’ that everyone knows that monetary policy controls financial\nmarkets, which keeps economies operating close to desired means. By contrast, last week’s entry\nshowed that excessive money printing creating ever more bad debt for over four decades reduced\ngrowth and has created the most fragile credit structure in history – making the desired\nnormalization of economies and financial markets impossible. For example, depression in\nEurope’s periphery appears to have corrected some imbalances, but little of the required\nrestructuring has occurred, so the apparent corrections would quickly reverse if growth resumed.\nMeanwhile, perceptions of the euro being an unfair zero sum game have caused a swift rise of\nnationalist parties in both core and peripheral nations. They embody the rising antipathy to the\nloss of sovereignty that is absolutely necessary to preserve the euro. Also Germany wants close\nrelationships with Russia while Eastern Europe wants protection from Russian domination. Such\ndifferences are irreconcilable. Chaos in Ukraine and the Russia sale of gas to China (which will aid\nAsian federation and undermine the petro dollar) are threatening to impede the flow of energy to\nEurope and raise prices. Nevertheless, Belief remains firm.\n\n\fSKENE’S SCENE\n\nRising dependency and leverage ratios combined with falling real incomes are stoking the currency\nwars. While the Fed assures markets that ultra-low rates will remain for some time, the Eurozone,\nAustralia, Canada, New Zealand, China, India, Brazil and South Korea would like to devalue their\ncurrencies, hoping it would solve their problems. However, devaluation can only buy time to fix\ndomestic problems. Politicians seldom use that time well and chronic devaluation, like chronic\nmoney printing, is reducing growth and living standards in many nations, especially in peripheral\nEurope and Latin America, but Belief remains firm.\nLiquidity has generally flowed from EM to DM since real interest rates began rising in 2012. The\nPBOC creating more of the excess liquidity roaming the globe seeking higher rates of return than\nany other central bank made it the biggest contributor to the Belief. A greatly overvalued currency\nand rapidly inflating credit bubble give China big incentives to devalue the yuan and generate an\noutflow of funds. Exchange rate manipulation and allowing more domestic capital outflows could\nlower the yuan, thereby discouraging capital inflows and raising domestic desires to export\ncapital.\nA falling yuan would raise Chinese current account surpluses. Capital exports would have to\nexceed them to lower domestic money growth to keep downward pressure on the yuan and\nforeign exchange reserves. The falling foreign exchange reserves would then release funds that\ncould be used to recapitalise banks. Devaluation would help in both reducing credit growth and\nrecapitalising banks, but would also entail the biggest contributor to the Belief reversing its stance\nto become its biggest saboteur.\nAfter the GFC, disintermediation raised Chinese credit growth above its extremely fast money\ngrowth. The PBOC is now trying to lower the rate of credit growth to nominal GDP growth to keep\nthe credit bubble from inflating further. Officially, nominal GDP growth was less than 10% in six of\nthe last seven quarters, yet the target for money growth remains 13%. This seems high, but the\nPBOC is re-intermediating shadow bank credit, which is pushing credit growth below money\ngrowth and raising domestic real interest rates. With ZIRP in many nations, high Chinese domestic\ninterest rates are aggravating capital inflows, so foreign exchange reserves rose again in Q1,\ncontrary to Chinese desires.\nTo reduce capital inflows, China manipulated the yuan down – challenging the widely held idea\nthat it must keep rising. But opening the capital account to domestic outflows is the only way\nChina can achieve its objectives of a truly floating yuan, reining in the credit bubble and\nrecapitalising banks without financial repression. If that opening were relatively free from\nbureaucratic interference, Chinese citizens probably would make maximum use of it. The\ncommensurate rundown of foreign exchange reserves would raise DM interest rates, probably high\nenough to cause recessions in Europe and North America. Meanwhile, the exported capital would\nraise DM equity and real estate markets – some to record overvaluations.\n\n2/4\n\n\fSKENE’S SCENE\n\nThe ensuing crash would certainly end Belief – but opening the capital account is being delayed\nindefinitely. Moreover, the recent increases in infrastructure spending, tax cuts to small business\nand continuing aggression along the 9 dash line show that growth and distraction from domestic\ndifficulties are taking precedence over reform. Opening the capital account to domestic outflows\nmay not occur for a long time, begging the question, “How long before the credit bubble\ncollapses?” which would also end Belief. The extremely rapid inflation of the bubble indicates a\ncouple of years.\nBelief created complacency in financial markets that contrasts sharply with the rising fragility of\ncredit structures (and by extension international trade as in the GFC) from rising dependency\nratios, falling incomes, dysfunctional governments, undercapitalised banks and over indebtedness\nin both public and private sectors. Lacy Hunt’s (VP Hoisington Investment Management) estimate\nof global debt at 435% of GDP indicates that risks to the credit structure and trade today exceed\nthose of the late 1920s – before the gold exchange standard collapsed in 1931. Meanwhile\ncomplacency, rivalling that immediately prior to WWI, rules –but the end is in sight.\n"

sumup.ProcessText(content, function(error, summary) {
	if(err) 
    console.log("Error :" = error);
	else 
    console.log(summary.summary);
});
```

The output of the above is:

```
"Belief created complacency in financial markets that contrasts sharply with the rising fragility of credit structures (and by extension international trade as in the GFC) from rising dependency ratios, falling incomes, dysfunctional governments, undercapitalised banks and over indebtedness in both public and private sectors. This seems high, but the PBOC is re-intermediating shadow bank credit, which is pushing credit growth below money growth and raising domestic real interest rates. By contrast, last week’s entry showed that excessive money printing creating ever more bad debt for over four decades reduced growth and has created the most fragile credit structure in history – making the desired normalization of economies and financial markets impossible. Politicians seldom use that time well and chronic devaluation, like chronic money printing, is reducing growth and living standards in many nations, especially in peripheral Europe and Latin America, but Belief remains firm."
```


# Configuration

- Supported langage 
  - ENG : English

- Supported stemmer 
  - PORTER : Porter stemmer
    
- Supported summarization algo 
  - ARTEX : Artex
    
# Tests

	$ npm test
  
### Contributors

Charles Durban <charlesdurban@me.com>

### License

MIT