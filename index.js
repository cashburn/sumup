'use strict';

/**
 * Seedalpha's text sumup creation
 */


/**
 * Module dependencies
 */

var Summarize     = require('./lib/summarize');
var PreProcessing = require('./lib/pre-processing');

/**
 * Library Constructor
 */

function Sumup(config){
  this.config = config || {
                      language          : 'ENG',
                      normalization : 'PORTER',
                      ATS           : 'ARTEX',
                      summarySize   : 4
                   };
}

Sumup.prototype.ProcessText = function(text, callback){
  var self = this;
  var preProcessing = new PreProcessing(this.config);
  preProcessing.ProcessText(text, function(err, res){
    if(err) 
      return callback(err);
    else {
      var summarize = new Summarize(self.config);
      summarize.ProcessText(res, function(error, result){
        if (error) {
          return callback(err);
        } else {
          callback(null, result);
        }
      }); 
    }
  });
};


exports = module.exports = Sumup;
