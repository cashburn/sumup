'use strict';

/**
 * Summarization
 */


/**
 * Module dependencies
 */

var Algo = require('./Algo');

/**
 * Module instanciation
 */

function Summarize(config){
  this.config = config;
}

/**
 * Methods
 */

Summarize.prototype.ProcessText = function(input, callback){
  
  var self = this;
  
  if (!self.config.ATS || !self.config.summarySize) {
    return callback(new Error('Missing arguments in config'));
  } else if (!input.split || !input.matrix) {
    return callback(new Error('text is missing pre-process results'));    
  } else {
    switch(self.config.ATS) {
      case 'ARTEX':
        Algo.Artex.GetSummary(input.split, input.matrix, self.config.summarySize, function(error, scores, summary){
          if (error || !scores || !summary) {
            return callback(new Error('An error has occured while summarizing the text ' + (error || "") ));
          } else {
            input.scores = scores;
            input.summary = summary;
            return callback(null, input);
          }
        });
        break;
        
      case '':
        //TODO : implement cortex
        break;
        
      case '':
        //TODO : implement enertex
        break;
        
      default:
        return callback(
          new Error('This algo doesn\'t exist yet. If you \'re willing to implement it, go ahead ;)!'));
    } 
  }
};

exports = module.exports = Summarize;