'use strict';

/**
 * Summarization ARTEX
 */


/**
 * Module dependencies
 */
var SortedArraySet = require("collections/sorted-array-set");
var math           = require('mathjs');

/**
 * Methods
 */

exports = module.exports.GetSummary = function(split, matrix, summarySize, callback){
  
  var summary = "";
  var scores  = new SortedArraySet();
  var count   = 0;
  var iLine   = 0;
  var jCol    = 0;
  var a       = [];
  var b       = [];
  
  for (iLine = 0 ; iLine <  matrix.length; iLine++){
    count = 0;
    for (jCol = 0 ; jCol <  matrix[0].length; jCol++){
      count += matrix[iLine][jCol].value;
    }
    a.push({'key':iLine, 'value':count/matrix[0].length});
  }

  for (jCol = 0 ; jCol <  matrix[0].length; jCol++){
    count = 0;
    for (iLine = 0 ; iLine <  matrix.length; iLine++){
      count += matrix[iLine][jCol].value;
    }
    b.push({'key':iLine, 'value':count/matrix.length});
  }

  for (iLine = 0 ; iLine <  matrix.length; iLine++){
    var score = 0;
    for (jCol = 0 ; jCol <  matrix[0].length; jCol++){
      score += matrix[iLine][jCol].value * b[jCol].value;
    }
    score *= a[iLine].value / math.sqrt(math.pow(matrix.length, 3) * math.pow(matrix[0].length, 5)); 
    scores.push({'key': iLine,'value': score});
  }

  scores = scores.sorted(function(a, b){
    return b.value-a.value;
  });

  count = 0;
  scores.forEach(function(item){
    if(count <= summarySize){
      summary += split[item.key].replace(/\n/g, " ").trim() + ". ";
    }
    count++;
  });
  return callback(null, scores, summary);  
};
